# nwdaf Release 16 NFLOAD 

This project implements NWDAF AnalyticInfo service APIs for NFLOAD. This 

- All the openapi generated code is part of [nnwdaf-analyticsinfo](gitlab.com/jacobphilip/nnwdaf_analyticsinfo) repo and r16_modified branch.

- Configurations are in the file [nwdaf.conf](./resources/nwdaf.conf), eventually this file will be converted as configmap.

- NWDAF Analytic info response contains CPU prediction from cpu model microservice and remaining values are dummy for now.


**Build & run**

`go build && ./nwdaf-service`

Note: nwdaf.conf should be in right place with the proper configurations.

**Request:**

``` bash
curl 127.0.0.1:8080/nnwdaf-analyticsinfo/v1/analytics/?event-id=NF_LOAD
```

**Response:**

```json
{
    "expiry": "2021-07-06T10:00:59.804286005+05:30",
    "nfLoadLevelInfos": [
        {
            "confidence": 90,
            "nfCpuUsage": 146,
            "nfInstanceId": "e6a48ce7-dc8b-40d8-a322-949f57602ffd",
            "nfStatus": {
                "statusRegistered": 20,
                "statusUnregistered": 10
            },
            "nfType": {
                "NFTypeAnyOf": "AMF"
            }
        }
    ],
    "start": "2021-07-05T10:00:59.804285804+05:30",
    "timeStampGen": "2021-07-05T10:00:59.804287668+05:30"
}
```



