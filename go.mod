module example.com/nwdaf-service

go 1.16

require (
	github.com/google/uuid v1.2.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/prometheus/common v0.30.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.6.1 // indirect
	gitlab.com/jacobphilip/nnwdaf_analyticsinfo v0.0.0-20210712131837-a556ffa57905
	google.golang.org/appengine v1.6.7 // indirect
)
