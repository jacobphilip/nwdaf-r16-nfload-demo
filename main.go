package main

import (
	"bytes"
	"context"
	"encoding/json"
	"example.com/nwdaf-service/metricsstorage"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

var MetricsStorage metricsstorage.TSDB

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	log.SetOutput(os.Stdout)
}

func main() {

	log.Printf("Staring NWDAF Services")
	analyticHandler := AnalyticHandler()

	configFile, err := os.Open("/opt/amcop/config/nwdaf.conf")
	if err != nil {
		log.Error("Failed to read nwdaf configuration")
		return
	}
	defer configFile.Close()

	// Read the configuration json
	byteValue, _ := ioutil.ReadAll(configFile)
	json.Unmarshal(byteValue, &analyticHandler.AnalyticsInfoConf)
	log.Infof("Analytics Config is: %+v", analyticHandler.AnalyticsInfoConf)

	// parse string, this is built-in feature of logrus
	logLevel, err := log.ParseLevel(analyticHandler.AnalyticsInfoConf.Loglevel)
	if err != nil {
		logLevel = log.DebugLevel
	}
	// set global log level
	log.SetLevel(logLevel)

	// Register to NRF
	MetricsStorage = new(metricsstorage.TempTSDB)
	MetricsStorage.InitializeMetricCollection(10, "cpu-1")
	go metricsstorage.FillCPUUsageHistory(MetricsStorage)
	analyticHandler.AssignMetricsStorage(MetricsStorage)
	registerToNRF(analyticHandler)
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/v1/models/cpu_usage/versions/1:predict", getDummyModelResp).Methods("POST")
	//router.HandleFunc("/nnwdaf-analyticsinfo/v1/analytics", analyticHandler.GetNWDAFAnalyticsHandler).Methods("GET")
	router.Handle("/nnwdaf-analyticsinfo/v1/analytics", analyticHandler)

	loggedRouter := handlers.LoggingHandler(os.Stdout, router)
	httpServer := &http.Server{
		Handler:      loggedRouter,
		Addr:         ":" + analyticHandler.AnalyticsInfoConf.Port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	//go metricsstorage.FillCPUUsageHistory(MetricsStorage)
	// Start server in a go routine.
	go func() {
		log.Fatal(httpServer.ListenAndServe())
	}()

	// Gracefull shutdown of the server,
	// create a channel and wait for SIGINT
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	log.Info("wait for signal")
	<-c
	log.Info("Bye Bye")
	httpServer.Shutdown(context.Background())
}

// register NWDAF endpoint to the NRF
func registerToNRF(analyticHandler *NWDAFAnalyticsInfo) {
	nrf_url := "http://" + analyticHandler.AnalyticsInfoConf.Nrf +
		"/nnrf-nfm/v1/nf-instances/" + analyticHandler.AnalyticsInfoConf.InstanceName

	var nrfPayload nrfRegister
	nrfPayload.NfInstanceId = analyticHandler.AnalyticsInfoConf.InstanceId
	nrfPayload.NfInstanceName = analyticHandler.AnalyticsInfoConf.InstanceName
	nrfPayload.NfType = "NWDAF"
	nrfPayload.NfStatus = "REGISTERED"
	//TODO Need to identify the right fields to pass NWDAF endpoint
	nrfPayload.Ipv4Addresses = []string{
		analyticHandler.AnalyticsInfoConf.Host + ":" + analyticHandler.AnalyticsInfoConf.Port}

	log.Infof("Registering NWDAF to NRF, URL : %s , payload %+v", nrf_url, nrfPayload)
	postBody, _ := json.Marshal(nrfPayload)
	req, err := http.NewRequest(http.MethodPut, nrf_url, bytes.NewBuffer(postBody))
	if err != nil {
		log.Fatalln(err)
	}
	client := &http.Client{}
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	resp, e := client.Do(req)
	if e != nil {
		log.Fatalln(e)
	}
	log.Infof("NWDAF registration to NRF successful. Status: %s", resp.Status)
}

// simulates cpu prediction model response
func getDummyModelResp(writer http.ResponseWriter, request *http.Request) {
	log.Printf("simulating Model Response")
	reqBody, _ := ioutil.ReadAll(request.Body)
	log.Infof("Model Request Body is %s", reqBody)
	//INPUT {"instances": [[[0.00347364], [0.00337148], [0.00398447],[0.00551696]]]}
	//OUTPUT {"predictions": [[0.0146191195]]}
	resp := &map[string][][]float32{
		"predictions": [][]float32{{0.0146191195}},
	}
	retval, _ := json.Marshal(&resp)
	log.Infof("CPU model returing json response: %s", retval)
	writer.Write(retval)
	writer.WriteHeader(http.StatusOK)
}

type nrfRegister struct {
	NfInstanceId   string   `json:"nfInstanceId"`
	NfInstanceName string   `json:"nfInstanceName"`
	NfType         string   `json:"nfType"`
	NfStatus       string   `json:"nfStatus"`
	Ipv4Addresses  []string `json:"ipv4Addresses"`
	Ipv6Addresses  []string `json:"ipv6Addresses"`
	FQDN           string   `json:"fqdn"`
}

