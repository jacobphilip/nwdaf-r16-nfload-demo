package metricsstorage

import (
	"encoding/json"
	"fmt"
	"github.com/prometheus/common/model"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

type  ResultFormat struct{
	Metric MetricFormat       `json:"metric"`
	Values []model.SamplePair `json:"values"`
}
type ResponseTemplate struct {
	Status string     `json:"status"`
	Data   DataFormat `json:"data"`
}

type DataFormat struct{
	ResultType string         `json:"resultType"`
	Result     []ResultFormat `json:"result"`
}

type MetricFormat struct{
	BetaKubernetesIoArch string `json:"beta_kubernetes_io_arch"`
	BetaKubernetesIoOs   string `json:"beta_kubernetes_io_os"`
	Container            string `json:"container"`
	Cpu string `json:"cpu"`
	Id string `json:"id"`
	Image string `json:"image"`
	Instance string `json:"instance"`
	Job                    string `json:"job"`
	KubernetesIoArch     string `json:"kubernetes_io_arch"`
	KubernetesIoHostname string `json:"kubernetes_io_hostname"`
	KubernetesIoOs       string `json:"kubernetes_io_os"`
	Name                 string `json:"name"`
	Namespace string `json:"namespace"`
	Pod string `json:"pod"`
}

type ValuesFormat struct{
	timestamp int64
	value string

}

func FillCPUUsageHistory(MetricsStorage TSDB){
	MetricsStorage.InitializeMetricCollection(10, "cpu")
	for
	{
		value := getCPUUsageHistory()
		MetricsStorage.AddValue(value)
		time.Sleep(10 * time.Second)
		log.Info(MetricsStorage.GetValues())
	}
}


func getCPUUsageHistory() float64 {
	fmt.Println("inisde cpu history")
	//postBodyBytes := bytes.NewBuffer([]byte(""))
	end := time.Now().Unix()
	endStr := strconv.FormatInt(end, 10)
	start := time.Now().Add(time.Duration(-5) * time.Minute).Unix()
	startStr := strconv.FormatInt(start, 10)
	resp, err := http.Get("http://192.168.102.88:31761/api/v1/query_range?" +
		"query=rate(container_cpu_usage_seconds_total%7Bnamespace%3D%22simulator%22%2C%20container%3D%22simulator%22%7D%5B5m%5D)" +
		"&start=" + startStr + "&end=" + endStr + "&step=5m")
	/*
		resp, err := http.Post("http://192.168.102.88:31761/api/v1/query_range?" +
			"avg(rate(container_cpu_usage_seconds_total{namespace=\"simulator\"}[1m]))" +
			"&start=1630067400&end=1630068495&step=15", "application/json", postBodyBytes ) */
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		bodyString := string(bodyBytes)
		log.Info(bodyString)
		var rt ResponseTemplate
		json.Unmarshal(bodyBytes, &rt)
		//model.
		log.Info(rt)
		return float64(rt.Data.Result[0].Values[0].Value)
	}

	return 0
}

