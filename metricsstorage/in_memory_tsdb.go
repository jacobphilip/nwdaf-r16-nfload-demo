package metricsstorage

type MetricCollection struct {
	_metrics []float64
	maxLength int
}

type TSDB interface {
	InitializeMetricCollection(maxLength int, name string)
	AddValue(value float64)
	GetValues() []float64
}

type TempTSDB struct {
	mCollection *MetricCollection
	name string
}
func (db *TempTSDB) InitializeMetricCollection(maxLength int, name string) {
	db.mCollection = new(MetricCollection)
	db.name = name
	db.mCollection.maxLength = maxLength
	db.mCollection._metrics = []float64{}
}

func (db *TempTSDB) AddValue(value float64){
	db.mCollection._metrics = append(db.mCollection._metrics, value)
	if len(db.mCollection._metrics) > db.mCollection.maxLength {
		db.mCollection._metrics = db.mCollection._metrics[1:]
	}
}

func (db *TempTSDB) GetValues() []float64 {
	return db.mCollection._metrics
}