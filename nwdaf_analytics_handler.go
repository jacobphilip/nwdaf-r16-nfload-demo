package main

import (
	"bytes"
	"encoding/json"
	"example.com/nwdaf-service/metricsstorage"
	"github.com/google/uuid"
	"io/ioutil"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	models "gitlab.com/jacobphilip/nnwdaf_analyticsinfo"
)


type NWDAFAnalyticsInfo struct {
	AnalyticsInfoConf AnalyticsInfoConfig
	MetricsStorage    metricsstorage.TSDB
}

type AnalyticsInfoConfig struct {
	Port          string `json:"port"`
	Host          string `json:"host"`
	Nrf           string `json:"nrf"`
	CpuPrediction string `json:"cpu_prediction"`
	Loglevel      string `json:"loglevel"`
	InstanceName  string `json:"instance_name"`
	InstanceId    string `json:"instance_id"`
}

func AnalyticHandler() *NWDAFAnalyticsInfo {
	return &NWDAFAnalyticsInfo{}
}

func (c NWDAFAnalyticsInfo) AssignMetricsStorage(db metricsstorage.TSDB){
	c.MetricsStorage = db
}

func (c NWDAFAnalyticsInfo) GetNWDAFAnalyticsHandler(w http.ResponseWriter, r *http.Request) {
	c.HandleAnalyticsRequest(w, r)
}

func (c NWDAFAnalyticsInfo) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c.HandleAnalyticsRequest(w, r)
}
func (c NWDAFAnalyticsInfo) HandleAnalyticsRequest(w http.ResponseWriter, r *http.Request) {
	eventId := r.FormValue("event-id")
	log.Debug("Received request to HandleAnalyticsRequest")
	log.Infof(" Event id is: %s", eventId)

	//Mandatory field check
	if len(eventId) == 0 {
		problemDetails := models.NewProblemDetails()
		problemDetails.SetTitle("Invalid Parameter")
		problemDetails.SetStatus(http.StatusBadRequest)
		problemDetails.SetCause("event-id is mandatory parameter")
		w.WriteHeader(http.StatusBadRequest)
		retval, _ := json.Marshal(problemDetails)
		w.Write(retval)
		return
	}

	// get cpu prediction
	avgCpuUsage, err := c.getAvgCPU()
	if err != nil {
		log.Fatalln("Error while calculating avg cpu", err)
	}

	nfload := models.NewNfLoadLevelInformationWithDefaults()
	nfload.SetNfCpuUsage(avgCpuUsage)
	//Except cpu, all the values are dummy just to fill the response
	nfload.NfInstanceId = uuid.New().String()
	nfload.SetNfType(models.AMF)

	nfStatus := models.NewNfStatusWithDefaults()
	nfStatus.SetStatusRegistered(80)
	nfStatus.SetStatusUnregistered(10)
	nfload.SetNfStatus(*nfStatus)
	nfload.SetConfidence(90)

	analyticData := models.NewAnalyticsData()
	analyticData.SetExpiry(time.Now().AddDate(0, 0, 1))
	analyticData.SetTimeStampGen(time.Now())

	analyticData.SetNfLoadLevelInfos([]models.NfLoadLevelInformation{*nfload})
	retVal, _ := analyticData.MarshalJSON()
	log.Infof("AnalyticInfo Response: %s", *analyticData)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write(retVal)
}

//call model micro service to get the result and modify output as per the spec
func (c NWDAFAnalyticsInfo) getAvgCPU() (int32, error) {
	//prepare the required input data, currently hardcoded and call model microservice
	cpuModelurl := c.AnalyticsInfoConf.CpuPrediction
	 if MetricsStorage == nil {
		MetricsStorage = new(metricsstorage.TempTSDB)
		MetricsStorage.InitializeMetricCollection(10, "cpu")
		go metricsstorage.FillCPUUsageHistory(c.MetricsStorage)
	}
	metricValues := MetricsStorage.GetValues()
	log.Infof("Metric values: %v", metricValues)
	/*postBody, _ := json.Marshal(map[string][][][]float32{
		"instances": metricValues,
	})*/
	//var v [][][]float64
	v := make([][][]float64, 1)
	v[0] = make([][]float64, len(metricValues))
	//v[0][0] = make([]float64, len(metricValues))
	for i, value := range metricValues{
		v[0][i] =  make([]float64, 1)
		v[0][i][0] = value
	}
	postBody, _ := json.Marshal(map[string][][][]float64{
		"instances": v,
	})

	postBodyBytes := bytes.NewBuffer(postBody)
	log.Infof("CPU Model Microserivce request URL: %s , PAYLOAD: %+v", cpuModelurl, string(postBody))
	resp, err := http.Post(cpuModelurl, "application/json", postBodyBytes)
	if err != nil {
		log.Fatalln("Error from cpu prediction model: ", err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	log.Infof("CPU model microservice response: %s", body)

	var modelResp = struct {
		Values [][]float32 `json:"predictions"`
	}{}
	json.Unmarshal(body, &modelResp)
	cpuPrediction := modelResp.Values[0][0]
	// This isn't the avg mean calculation, just to show the numbers on response
	var avgCpuUsage = int32(cpuPrediction * 100)
	return avgCpuUsage, nil
}
